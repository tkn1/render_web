#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Answer {
    #[prost(bool, tag = "1")]
    pub status: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Bounds {
    #[prost(uint64, tag = "1")]
    pub width: u64,
    #[prost(uint64, tag = "2")]
    pub height: u64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SceneId {
    #[prost(uint64, tag = "1")]
    pub id: u64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Point {
    #[prost(float, tag = "1")]
    pub x: f32,
    #[prost(float, tag = "2")]
    pub y: f32,
    #[prost(float, tag = "3")]
    pub z: f32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Line {
    #[prost(message, optional, tag = "1")]
    pub point_1: ::core::option::Option<Point>,
    #[prost(message, optional, tag = "2")]
    pub point_2: ::core::option::Option<Point>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Triangle {
    #[prost(message, optional, tag = "1")]
    pub point_1: ::core::option::Option<Point>,
    #[prost(message, optional, tag = "2")]
    pub point_2: ::core::option::Option<Point>,
    #[prost(message, optional, tag = "3")]
    pub point_3: ::core::option::Option<Point>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Color {
    #[prost(uint64, tag = "1")]
    pub r: u64,
    #[prost(uint64, tag = "2")]
    pub g: u64,
    #[prost(uint64, tag = "3")]
    pub b: u64,
    #[prost(uint64, tag = "4")]
    pub a: u64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RenderPointRequest {
    #[prost(message, optional, tag = "1")]
    pub scene_id: ::core::option::Option<SceneId>,
    #[prost(message, optional, tag = "2")]
    pub point: ::core::option::Option<Point>,
    #[prost(message, optional, tag = "3")]
    pub color: ::core::option::Option<Color>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RenderLineRequest {
    #[prost(message, optional, tag = "1")]
    pub scene_id: ::core::option::Option<SceneId>,
    #[prost(message, optional, tag = "2")]
    pub line: ::core::option::Option<Line>,
    #[prost(message, optional, tag = "3")]
    pub color: ::core::option::Option<Color>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RenderTriangleRequest {
    #[prost(message, optional, tag = "1")]
    pub scene_id: ::core::option::Option<SceneId>,
    #[prost(message, optional, tag = "2")]
    pub triangle: ::core::option::Option<Triangle>,
    #[prost(message, optional, tag = "3")]
    pub color: ::core::option::Option<Color>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RenderedImage {
    #[prost(bytes = "vec", tag = "1")]
    pub image: ::prost::alloc::vec::Vec<u8>,
}
