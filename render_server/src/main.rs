mod scene;
use apng::*;
use scene::Bounds;
use scene::Color;
use scene::Line;
use scene::Point;
use scene::RenderLine;
use scene::RenderPoint;
use scene::RenderTriangle;
use scene::Scene;
use scene::Triangle;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;

fn main() {
    println!("Start!");
    // rotation_test();
    // depth_test();
    // circle_test(Point::new(0.0, 0.0, 2.0), 1.0, 0.05);
}
// #[tokio::main]
// async fn main() -> Result<(), Box<dyn std::error::Error>> {
//     dotenv().ok();
//     env_logger::init();

//     info!("Starting Solar System info server");

//     let addr = std::env::var("GRPC_SERVER_ADDRESS")?.parse()?;

//     let pool = create_connection_pool();
//     run_migrations(&pool);

//     let solar_system_info = SolarSystemInfoService { pool };
//     let svc = SolarSystemInfoServer::new(solar_system_info);

//     Server::builder().add_service(svc).serve(addr).await?;

//     Ok(())
// }