use rand::Rng;

pub trait RenderPoint {
    fn render(&mut self, point: &Point, color: &Color);
}
pub trait RenderLine {
    fn render(&mut self, line: &Line, color: &Color);
}

pub trait RenderTriangle {
    fn render(&mut self, triangle: &Triangle, fill: bool, color: &Color);
}

impl RenderPoint for Scene {
    fn render(&mut self, point: &Point, color: &Color) {
        let point = self.projection(point);
        self.set_pixel(point, color);
    }
}

impl RenderLine for Scene {
    fn render(&mut self, line: &Line, color: &Color) {
        // let point_1 = self.projection(&line.points[0]);
        // let point_2 = self.projection(&line.points[1]);
        // println!("RenderLine {:?}", line);

        let point_1 = self.plane_line_intersection((0.0, 0.0, 1.0), line);
        let point_2 = if point_1.z < line.points[0].z {
            line.points[0]
        } else {
            line.points[1]
        };

        println!("RenderLine {:?} {:?}", point_1, point_2);

        let pixel_1 = self.projection(&point_1);
        let pixel_2 = self.projection(&point_2);

        println!("RenderLine {:?} {:?}", pixel_1, pixel_2);
        self.draw_line(pixel_1, pixel_2, color);
    }
}

impl RenderTriangle for Scene {
    fn render(&mut self, triangle: &Triangle, fill: bool, color: &Color) {
        // println!("triangle {:?}", triangle);
        // for (index, point) in triangle.points.iter().enumerate() {
        //     if point.z < 0.0 {
        //         return;
        //     }
        // }
        // let triangle = triangle.apply_scale();
        // println!("triangle scaled {:?}", triangle);
        let norm = triangle.normal();
        let centre = triangle.centroid();
        let light_vec = Vector::from_point(&centre);
        let norm_line = norm.shift(&centre);
        // println!("norm_line {:?}", norm_line);

        let color = self.shading(triangle, color);
        let point_1 = self.projection(&triangle.points[0]);
        let point_2 = self.projection(&triangle.points[1]);
        let point_3 = self.projection(&triangle.points[2]);
        if fill {
            self.fill_triangle(point_1, point_2, point_3, &color);
        } else {
            self.draw_triangle(point_1, point_2, point_3, &color);
        }
        <Scene as RenderLine>::render(self, &norm_line, &Color::rand());
    }
}

pub struct Scene {
    bounds: Bounds,
    frame_buffer: Vec<u8>,
    capacity: usize,
}

impl Scene {
    pub fn new(bounds: Bounds) -> Self {
        let capacity = bounds.height * bounds.width * 4;
        assert_ne!(capacity, 0);
        Self {
            bounds: bounds,
            frame_buffer: vec![0; capacity],
            capacity: capacity,
        }
    }
    fn shading(&mut self, triangle: &Triangle, color: &Color) -> Color {
        let norm = triangle.normal().normalize();
        let centre = triangle.centroid();
        let light_vec = Vector::from_point(&centre);
        let norm_line = norm.shift(&centre);
        <Scene as RenderLine>::render(self, &norm_line, &Color::rand());
        let angle = norm.angle(&light_vec);
        let brightness = (f64::cos(angle) * std::f64::consts::PI) / 180.0;
        let ambient = 0.0;
        let final_brightness = brightness.max(0.0) + ambient;
        println!("brightness {}", brightness.max(0.0));
        // let result_color = color.apply_brightness(final_brightness);
        let result_color = color.apply_brightness(final_brightness);
        result_color
    }

    fn set_pixel(&mut self, point: (usize, usize), color: &Color) {
        assert_ne!(self.frame_buffer.len(), 0);
        if point.0 >= self.bounds.width || point.1 >= self.bounds.height {
            return;
        }
        let x = 4 * point.0;
        let y = 4 * point.1;

        let index = x * self.bounds.width + y;
        let slice_color = Color::u8(&color);
        for i in 0..slice_color.len() {
            self.frame_buffer[index + i] = slice_color[i];
        }
    }
    fn draw_line(&mut self, point_1: (usize, usize), point_2: (usize, usize), color: &Color) {
        extern crate bresenham;
        use bresenham::Bresenham;
        let alg = Bresenham::new(
            (point_1.0 as isize, point_1.1 as isize),
            (point_2.0 as isize, point_2.1 as isize),
        );
        for (x, y) in alg {
            self.set_pixel((x as usize, y as usize), color);
        }
    }
    fn draw_triangle(
        &mut self,
        point_1: (usize, usize),
        point_2: (usize, usize),
        point_3: (usize, usize),
        color: &Color,
    ) {
        self.draw_line(point_1, point_2, color);
        self.draw_line(point_1, point_3, color);
        self.draw_line(point_3, point_2, color);
    }
    fn fill_triangle(
        &mut self,
        point_1: (usize, usize),
        point_2: (usize, usize),
        point_3: (usize, usize),
        color: &Color,
    ) {
        let mut vertices = vec![point_1, point_2, point_3];
        vertices.sort_by_key(|k| k.1);

        let v1 = vertices[0];
        let v2 = vertices[1];
        let v3 = vertices[2];

        /* here we know that v1.y <= v2.y <= v3.y */
        /* check for trivial case of bottom-flat triangle */
        if v2.1 == v3.1 {
            self.fill_bottom_flat_triangle(v1, v2, v3, color);
        }
        /* check for trivial case of top-flat triangle */
        else if v1.1 == v2.1 {
            self.fill_top_flat_triangle(v1, v2, v3, color);
        } else {
            /* general case - split the triangle in a topflat and bottom-flat one */
            let v4_x = v1.0 as f64
                + ((v2.1 as f64 - v1.1 as f64) / (v3.1 as f64 - v1.1 as f64))
                    * (v3.0 as f64 - v1.0 as f64) as f64;
            let v4 = (v4_x as usize, v2.1);
            self.fill_bottom_flat_triangle(v1, v4, v2, color);

            self.fill_top_flat_triangle(v2, v4, v3, color);
        }
    }
    fn fill_bottom_flat_triangle(
        &mut self,
        point_1: (usize, usize),
        point_2: (usize, usize),
        point_3: (usize, usize),
        color: &Color,
    ) {
        assert_eq!(point_2.1, point_3.1);

        let invslope_1 = (point_2.0 as isize - point_1.0 as isize) as f64
            / (point_2.1 as isize - point_1.1 as isize) as f64;
        let invslope_2 = (point_3.0 as isize - point_1.0 as isize) as f64
            / (point_3.1 as isize - point_1.1 as isize) as f64;

        let mut curx_1 = point_1.0 as f64;
        let mut curx_2 = point_1.0 as f64;

        for scanline_y in point_1.1..point_2.1 {
            self.draw_line(
                (curx_1 as usize, scanline_y),
                (curx_2 as usize, scanline_y),
                color,
            );
            curx_1 += invslope_1;
            curx_2 += invslope_2;
        }
    }
    fn fill_top_flat_triangle(
        &mut self,
        point_1: (usize, usize),
        point_2: (usize, usize),
        point_3: (usize, usize),
        color: &Color,
    ) {
        assert_eq!(point_2.1, point_1.1);

        let invslope_1 = (point_3.0 as isize - point_1.0 as isize) as f64
            / (point_3.1 as isize - point_1.1 as isize) as f64;
        let invslope_2 = (point_3.0 as isize - point_2.0 as isize) as f64
            / (point_3.1 as isize - point_2.1 as isize) as f64;

        let mut curx_1 = point_3.0 as f64;
        let mut curx_2 = point_3.0 as f64;

        for scanline_y in (point_1.1..point_3.1 - 1).rev() {
            self.draw_line(
                (curx_1 as usize, scanline_y),
                (curx_2 as usize, scanline_y),
                color,
            );
            curx_1 -= invslope_1;
            curx_2 -= invslope_2;
        }
    }
    pub fn clear(&mut self) {
        for val in self.frame_buffer.iter_mut() {
            *val = 0;
        }
    }
    pub fn dump(&self, name: String) {
        use std::fs::File;
        use std::io::BufWriter;
        use std::path::Path;

        // let now = std::time::Instant::now();

        let path = Path::new(&name);
        let file = File::create(path).unwrap();
        let ref mut w = BufWriter::new(file);
        let mut encoder = png::Encoder::new(w, self.bounds.width as u32, self.bounds.height as u32); // Width is 2 pixels and height is 1.
        encoder.set_color(png::ColorType::Rgba);
        encoder.set_depth(png::BitDepth::Eight);
        // encoder.set_trns(vec![0xFFu8, 0xFFu8, 0xFFu8, 0xFFu8]);
        encoder.set_source_gamma(png::ScaledFloat::from_scaled(45455)); // 1.0 / 2.2, scaled by 100000
        encoder.set_source_gamma(png::ScaledFloat::new(1.0 / 2.2)); // 1.0 / 2.2, unscaled, but rounded
        let source_chromaticities = png::SourceChromaticities::new(
            // Using unscaled instantiation here
            (0.31270, 0.32900),
            (0.64000, 0.33000),
            (0.30000, 0.60000),
            (0.15000, 0.06000),
        );

        encoder.set_source_chromaticities(source_chromaticities);
        let mut writer = encoder.write_header().unwrap();
        writer
            .write_image_data(self.frame_buffer.as_slice())
            .unwrap();
    }
    fn projection(&self, point: &Point) -> (usize, usize) {
        // let l = point.x;
        // let m = point.y;
        // let n = point.z;
        // let x = l / n;
        // let y = m / n;
        let line = Line::new(&Point::new(0.0, 0.0, 0.0), point);
        let projection = self.plane_line_intersection((0.0, 0.0, 1.0), &line);
        let x_coord = (((projection.x + 1.0) / 2.0) * (self.bounds.width as f64)) as usize;
        let y_coord = (((projection.y + 1.0) / 2.0) * (self.bounds.height as f64)) as usize;
        // println!("projection {:?} {:?} {:?}", projection, point, ( x_coord, y_coord ));
        (x_coord, y_coord)
    }
    fn plane_line_intersection(&self, plane: (f64, f64, f64), line: &Line) -> Point {
        let l = line.points[1].x - line.points[0].x;
        let m = line.points[1].y - line.points[0].y;
        let n = line.points[1].z - line.points[0].z;
        // println!("plane_line_intersection l {:?} m {:?} n {:?}", l, m, n);
        // let plane_x = plane.0;
        // let plane_y = plane.1;
        let plane_z = plane.2;
        // let r_x = (l / m) * (plane_y - line.points[0].y) + line.points[0].x;
        // let r_y = (m / n) * (plane_z - line.points[0].z) + line.points[0].y;
        // let r_z = (n / l) * (plane_x - line.points[0].x) + line.points[0].z;
        let r_x = (l / n) * (plane_z - line.points[0].z) + line.points[0].x;
        let r_y = (m / n) * (plane_z - line.points[0].z) + line.points[0].y;
        let r_z = plane_z;
        let point = Point::new(r_x, r_y, r_z);
        point
    }
}

pub struct Color {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
}
impl Color {
    pub fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self {
            r: r,
            g: g,
            b: b,
            a: a,
        }
    }
    pub fn rand() -> Self {
        let r = rand::thread_rng().gen_range(0..255) as u8;
        let g = rand::thread_rng().gen_range(0..255) as u8;
        let b = rand::thread_rng().gen_range(0..255) as u8;

        Self {
            r: r,
            g: g,
            b: b,
            a: 255,
        }
    }
    fn u8(&self) -> Vec<u8> {
        vec![self.r, self.g, self.b, self.a]
    }
    pub fn apply_brightness(&self, brightness: f64) -> Color {
        let r = self.r as f64 * brightness;
        let g = self.g as f64 * brightness;
        let b = self.b as f64 * brightness;
        Color::new(r as u8, g as u8, b as u8, self.a)
    }
}

pub struct Bounds {
    width: usize,
    height: usize,
}

impl Bounds {
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            width: width,
            height: height,
        }
    }
}
#[derive(Debug, Copy, Clone)]
pub struct Point {
    x: f64,
    y: f64,
    z: f64,
}

impl Point {
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x: x, y: y, z: z }
    }
    pub fn new_blank() -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }
    pub fn rand() -> Self {
        let x = rand::thread_rng().gen_range(0..100) as f64 / 100.0;
        let y = rand::thread_rng().gen_range(0..100) as f64 / 100.0;
        let z = rand::thread_rng().gen_range(0..100) as f64 / 100.0;
        Self { x: x, y: y, z: z }
    }
    pub fn get_denormalized_coords(&self, bounds: &Bounds) -> (usize, usize) {
        let x = (self.x * bounds.width as f64) as usize;
        let y = (self.y * bounds.height as f64) as usize;
        (x, y)
    }
    pub fn rotate_around_x(&self, origin: &Point, angle: f64) -> Point {
        // y''' = y'' * cos c - z'' * sin c
        // z''' = y'' * sin c + z'' * cos c
        let angle = (angle * std::f64::consts::PI) / 180.0;

        let qy = origin.y + angle.cos() * (self.y - origin.y) - angle.sin() * (self.z - origin.z);
        let qz = origin.z + angle.sin() * (self.y - origin.y) + angle.cos() * (self.z - origin.z);
        let point = Point::new(self.x, qy, qz);
        point
    }
    pub fn rotate_around_y(&self, origin: &Point, angle: f64) -> Point {
        // x'' = x'*cos b - z' * sin b;
        // z'' = x'*sin b + z' * cos b;
        let angle = (angle * std::f64::consts::PI) / 180.0;

        let qx = origin.x + angle.cos() * (self.x - origin.x) - angle.sin() * (self.z - origin.z);
        let qz = origin.z + angle.sin() * (self.x - origin.x) + angle.cos() * (self.z - origin.z);
        let point = Point::new(qx, self.y, qz);
        point
    }
    pub fn rotate_around_z(&self, origin: &Point, angle: f64) -> Point {
        // x' = x*cos a - y*sin a;
        // y' = x*sin a + y*cos a;

        let angle = (angle * std::f64::consts::PI) / 180.0;
        let qx = origin.x + angle.cos() * (self.x - origin.x) - angle.sin() * (self.y - origin.y);
        let qy = origin.y + angle.sin() * (self.x - origin.x) + angle.cos() * (self.y - origin.y);
        let point = Point::new(qx, qy, self.z);
        point
    }
    pub fn x(&self) -> f64 {
        self.x
    }
    pub fn y(&self) -> f64 {
        self.y
    }
    pub fn z(&self) -> f64 {
        self.z
    }
}
#[derive(Debug, Copy, Clone)]
pub struct Line {
    points: [Point; 2],
}

impl Line {
    pub fn new(point_1: &Point, point_2: &Point) -> Self {
        let centre: Point = Line::centre_calculate(&point_1, &point_2);
        Self {
            points: [point_1.clone(), point_2.clone()],
        }
    }
    pub fn rand() -> Self {
        Self {
            points: [Point::rand(), Point::rand()],
        }
    }
    fn centre_calculate(point_1: &Point, point_2: &Point) -> Point {
        let point = Point::new(
            (point_1.x + point_2.x) / 2.0,
            (point_1.y + point_2.y) / 2.0,
            (point_1.z + point_2.z) / 2.0,
        );
        point
    }
}
#[derive(Debug, Copy, Clone)]
pub struct Triangle {
    points: [Point; 3],
    // centre: Point
}

impl Triangle {
    pub fn new(point_1: &Point, point_2: &Point, point_3: &Point) -> Self {
        Self {
            points: [point_1.clone(), point_2.clone(), point_3.clone()],
        }
    }
    pub fn rand() -> Self {
        Self {
            points: [Point::rand(), Point::rand(), Point::rand()],
        }
    }
    pub fn rotate_around_x(&self, origin: &Point, angle: f64) -> Triangle {
        let mut points: [Point; 3] = [Point::new_blank(); 3];
        for (index, point) in self.points.iter().enumerate() {
            points[index] = Point::rotate_around_y(&point, origin, angle);
        }
        Triangle::new(&points[0], &points[1], &points[2])
    }
    pub fn rotate_around_y(&self, origin: &Point, angle: f64) -> Triangle {
        let mut points: [Point; 3] = [Point::new_blank(); 3];
        for (index, point) in self.points.iter().enumerate() {
            points[index] = Point::rotate_around_y(&point, origin, angle);
        }
        Triangle::new(&points[0], &points[1], &points[2])
    }
    pub fn rotate_around_z(&self, origin: &Point, angle: f64) -> Triangle {
        let mut points: [Point; 3] = [Point::new_blank(); 3];
        for (index, point) in self.points.iter().enumerate() {
            points[index] = Point::rotate_around_z(&point, origin, angle);
        }
        Triangle::new(&points[0], &points[1], &points[2])
    }

    pub fn get_points(&self) -> [Point; 3] {
        self.points.clone()
    }
    pub fn centroid(&self) -> Point {
        let x = (self.points[0].x + self.points[1].x + self.points[2].x) / 3.0;
        let y = (self.points[0].y + self.points[1].y + self.points[2].y) / 3.0;
        let z = (self.points[0].z + self.points[1].z + self.points[2].z) / 3.0;
        Point::new(x, y, z)
    }
    pub fn set_anchor(&self, anchor: &Point) -> Triangle {
        let centroid = self.centroid();
        let mut points: [Point; 3] = [Point::new_blank(); 3];
        let delta_x = centroid.x - anchor.x;
        let delta_y = centroid.y - anchor.y;
        let delta_z = centroid.z - anchor.z;

        // println!("{:?}", self);
        // println!("centroid {:?}", centroid);
        // println!("anchor {:?}", anchor);
        // println!("{}", delta_x);
        // println!("{}", delta_y);
        // println!("{}", delta_z);
        for (index, point) in self.points.iter().enumerate() {
            points[index] = Point::new(point.x - delta_x, point.y - delta_y, point.z - delta_z);
        }
        Triangle::new(&points[0], &points[1], &points[2])
        // let centroid = self.centroid();
        // println!("centroid {:?}", centroid);
        // assert_eq!(self.centroid().x, anchor.x);
        // assert_eq!(self.centroid().y, anchor.y);
        // assert_eq!(self.centroid().z, anchor.z);
    }
    pub fn apply_scale(&self) -> Triangle {
        let centroid = self.centroid();
        // println!("centroid {:?}", centroid);
        let mut points: [Point; 3] = [Point::new_blank(); 3];
        let scale = 1.0 / self.centroid().z;
        // println!("scale {:?}", scale);
        for (index, point) in self.points.iter().enumerate() {
            points[index] = Point::new(point.x * scale, point.y * scale, point.z);
        }
        Triangle::new(&points[0], &points[1], &points[2])
    }
    pub fn normal(&self) -> Vector {
        let v_1 = Vector::from_points(&self.points[0], &self.points[1]).normalize();
        let v_2 = Vector::from_points(&self.points[0], &self.points[2]).normalize();
        let norm = v_1.cross_product(&v_2);
        Vector::normalize(&norm)
        // let centre = triangle.points[0];
        // let light_vec = centre;
        // let norm_line = Line::new(&norm, &centre);
        // let norm_line = self.normalize_vec(&norm_line);
        // let norm_line = self.shift_vec(&norm_line, &centre);
        // println!("norm_line {:?}", norm_line);
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Vector {
    x: f64,
    y: f64,
    z: f64,
}

impl Vector {
    pub fn from_point(coords: &Point) -> Self {
        Self {
            x: coords.x,
            y: coords.y,
            z: coords.z,
        }
    }
    pub fn from_points(point_1: &Point, point_2: &Point) -> Self {
        let point = Point::new(
            point_2.x - point_1.x,
            point_2.y - point_1.y,
            point_2.z - point_1.z,
        );
        Vector::from_point(&point)
    }
    pub fn from_coords(x: f64, y: f64, z: f64) -> Self {
        Self { x: x, y: y, z: z }
    }
    pub fn cross_product(&self, other: &Vector) -> Vector {
        let v1 = self;
        let v2 = other;
        let n_x = v1.y * v2.z - v1.z * v2.y;
        let n_y = v1.z * v2.x - v1.x * v2.z;
        let n_z = v1.x * v2.y - v1.y * v2.x;
        Vector::from_coords(n_x, n_y, n_z)
    }

    pub fn dot_product(&self, other: &Vector) -> f64 {
        let v1 = self;
        let v2 = other;
        let r = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
        let a_abs = f64::sqrt(v1.x.powi(2) + v1.y.powi(2) + v1.z.powi(2));
        let b_abs = f64::sqrt(v2.x.powi(2) + v2.y.powi(2) + v2.z.powi(2));
        let r = r / (a_abs * b_abs);
        r
    }
    pub fn length(&self) -> f64 {
        let length = (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt();
        length
    }
    pub fn normalize(&self) -> Vector {
        // let factor = 10000.0;
        let factor = 1.0;
        // let x = self.x / factor;
        // let y = self.y / factor;
        // let z = self.z / factor;
        let x = self.x;
        let y = self.y;
        let z = self.z;
        let length = (x.powi(2) + y.powi(2) + z.powi(2)).sqrt() * factor;
        // let length = x.powi(2) + y.powi(2) + z.powi(2);

        let x = x / length;
        let y = y / length;
        let z = z / length;

        // let length = x.powi(2) + y.powi(2) + z.powi(2);
        // assert_eq!(length, 1.0);
        let vector = Vector::from_coords(x, y, z);
        assert_eq!(vector.length().round(), 1.0);
        vector
    }
    pub fn shift(&self, shift: &Point) -> Line {
        let point_1 = Point::new(shift.x, shift.y, shift.z);
        let point_2 = Point::new(self.x + shift.x, self.y + shift.y, self.z + shift.z);
        Line::new(&point_1, &point_2)
    }
    pub fn angle(&self, other: &Vector) -> f64 {
        let v_1 = self.normalize();
        let v_2 = other.normalize();
        // let v_1 = self;
        // let v_2 = other;
        let dot_product = v_1.dot_product(&v_2);
        let length = v_1.length() * v_2.length();
        let angle = (f64::acos(dot_product / length) * 180.0) / std::f64::consts::PI;
        println!("{:?}", angle);
        angle
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use apng::*;
    use std::fs::File;
    use std::io::BufWriter;
    use std::path::Path;
    fn animation(files: Vec<String>, name: &String) {
        let mut png_images: Vec<PNGImage> = Vec::new();
        for f in files.iter() {
            png_images.push(apng::load_png(f).unwrap());
        }

        let path = Path::new(name);
        let mut out = BufWriter::new(File::create(path).unwrap());

        let config = apng::create_config(&png_images, None).unwrap();
        let mut encoder = Encoder::new(&mut out, config).unwrap();
        let frame = Frame {
            delay_num: Some(0),
            delay_den: Some(0),
            ..Default::default()
        };

        match encoder.encode_all(png_images, Some(&frame)) {
            Ok(_n) => println!("success"),
            Err(err) => eprintln!("{}", err),
        }
    }
    fn circle_movement(centre: Point, radius: f64, delta: f64) {
        let now = std::time::Instant::now();
        let mut scene = Scene::new(Bounds::new(1000, 1000));
        let triangle = Triangle::rand();
        let color = Color::rand();

        let mut rendered_scenes: Vec<String> = Vec::new();

        let steps = (2.0 * radius / delta) as usize;
        for step in 0..steps {
            let str_name = "point".to_string() + &step.to_string() + &".png".to_string();
            let x = centre.x() - radius + delta * step as f64;
            let root = f64::abs(f64::powi(x - centre.x(), 2) - f64::powi(radius, 2));
            let z = f64::sqrt(root) + centre.z();
            // println!("{} {}", x, z);
            let p = Point::new(x, 0.0, z);
            let triangle = triangle.set_anchor(&p);

            <Scene as RenderTriangle>::render(&mut scene, &triangle, true, &color);
            Scene::dump(&mut scene, str_name.clone());
            rendered_scenes.push(str_name.clone());
            scene.clear();
        }

        for step in (0..steps).rev() {
            let str_name = "point".to_string() + &(step + steps).to_string() + &".png".to_string();
            let x = centre.x() - radius + delta * step as f64;
            let root = f64::abs(f64::powi(x - centre.x(), 2) - f64::powi(radius, 2));
            let z = f64::abs(f64::sqrt(root) - centre.z());
            // println!("{} {}", x, z);
            let p = Point::new(x, 0.0, z);
            let triangle = triangle.set_anchor(&p);

            <Scene as RenderTriangle>::render(&mut scene, &triangle, true, &color);
            Scene::dump(&mut scene, str_name.clone());
            rendered_scenes.push(str_name.clone());
            scene.clear();
        }
        animation(rendered_scenes, &"circle.png".to_string());
    }

    #[test]
    #[ignore]
    fn test_normal() {
        let triangle = Triangle::rand();
        let norm = triangle.normal();
        let vec_1 = Vector::from_points(&triangle.points[0], &triangle.points[1]);
        let vec_2 = Vector::from_points(&triangle.points[0], &triangle.points[2]);
        let vec_3 = Vector::from_points(&triangle.points[2], &triangle.points[0]);
        println!("{:?}", vec_1);
        println!("{:?}", vec_2);
        println!("{:?}", vec_3);
        println!("{:?}", norm);
        // let sum = vec_1.angle(&vec_2).abs() + vec_1.angle(&vec_3).abs() + vec_3.angle(&vec_2).abs();
        // assert_eq!(sum.round(), 180.0);
        let angle_1 = vec_1.angle(&norm);
        let angle_2 = vec_2.angle(&norm);
        let angle_3 = vec_3.angle(&norm);
        assert_eq!(angle_1.round(), 90.0);
        assert_eq!(angle_2.round(), 90.0);
        assert_eq!(angle_3.round(), 90.0);
    }
    #[test]
    #[ignore]
    fn test_circle() {
        circle_movement(Point::new(0.0, 0.0, 2.0), 1.0, 0.5);
    }
    #[test]
    #[ignore]
    fn depth_test() {
        let now = std::time::Instant::now();
        let mut scene = Scene::new(Bounds::new(1000, 1000));
        let point_1 = Point::new(0.3, 0.3, 0.5);
        let point_2 = Point::new(0.5, 0.3, 0.5);
        let point_3 = Point::new(0.4, 0.5, 0.5);
        let triangle = Triangle::new(&point_1, &point_2, &point_3);
        let color = Color::rand();
        let mut rendered_scenes: Vec<String> = Vec::new();
        let scenes = 100;
        for range in 1..scenes {
            let str_name = "point".to_string() + &range.to_string() + &".png".to_string();
            let a = Point::new(0.5, 0.5, range as f64);
            let triangle = triangle.set_anchor(&a);
            <Scene as RenderTriangle>::render(&mut scene, &triangle, true, &color);
            Scene::dump(&mut scene, str_name.clone());
            rendered_scenes.push(str_name.clone());
            scene.clear();
            println!(
                "rendering done for range: {} time: {}",
                range,
                now.elapsed().as_millis()
            );
        }
        animation(rendered_scenes, &"depth.png".to_string());
    }
    #[test]
    #[ignore]
    fn rotation_test() {
        let now = std::time::Instant::now();

        let mut scene = Scene::new(Bounds::new(1000, 1000));
        // let point_1 = Point::new(3.0, 3.0, 5.0);
        // let point_2 = Point::new(5.0, 3.0, 5.0);
        // let point_3 = Point::new(4.0, 5.0, 5.0);

        let point_1 = Point::new(3.0, 3.0, 15.0);
        let point_2 = Point::new(-3.0, -3.0, 25.0);
        let point_3 = Point::new(4.0, 5.0, 5.0);
        let triangle = Triangle::new(&point_1, &point_2, &point_3);
        // let triangle = Triangle::rand();
        let origin = &triangle.get_points()[0];
        let color = Color::rand();
        let mut rendered_scenes: Vec<String> = Vec::new();
        let degrees = 360;
        for angle in 0..degrees {
            // <Scene as RenderPoint>::render(&mut scene, &origin, &color);

            // <Scene as RenderTriangle>::render(&mut scene, &triangle, true, &color);

            // let rot_triangle = triangle.rotate_around_x(&origin, angle as f64);
            // <Scene as RenderTriangle>::render(&mut scene, &rot_triangle, true, &color);

            let rot_triangle = triangle.rotate_around_y(&origin, angle as f64);
            <Scene as RenderTriangle>::render(&mut scene, &rot_triangle, true, &color);

            // let rot_triangle = triangle.rotate_around_z(&origin, angle as f64);
            // <Scene as RenderTriangle>::render(&mut scene, &rot_triangle, true, &color);

            println!(
                "rendering done for angle: {} time: {}",
                angle,
                now.elapsed().as_millis()
            );
            let str_name = "point".to_string() + &angle.to_string() + &".png".to_string();
            Scene::dump(&mut scene, str_name.clone());
            rendered_scenes.push(str_name.clone());
            scene.clear();
        }
        animation(rendered_scenes, &"rotation.png".to_string());
    }
}
