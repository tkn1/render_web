fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
    .build_client(false)
    .build_server(false)
    .out_dir("../render_server/src")
    .compile(
        &["../proto/renderer.proto"],
        &["../proto"],
    )?;
    Ok(())
}